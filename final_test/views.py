from django.shortcuts import redirect,render
from final_test.models import Quest,Answer
from django.utils import timezone


def home_page(request):
    quest=Quest.objects.all()
    now = timezone.now()
    m3=[]
    m6=[]
    month=0
    if Quest.objects.count() != 0:
        for quests in quest:
            if now.month < quests.pub_date.month :
               month = 12+now.month
            if month - quests.pub_date.month <= 3 :
               m3.append(quests.id)
            if month - quests.pub_date.month <= 6 :
               m6.append(quests.id)
    return render(request, 'home.html',{'quest': quest,'count': Quest.objects.count(),'now':now ,'m3':m3,'m6':m6})

def add_Q(request):
    if request.method == 'POST':
        questions = request.POST.get('your_question')
        answers1 = request.POST.get('your_ans1')
        answers2 = request.POST.get('your_ans2')           
        Quest.objects.create(question=questions, answer1=answers1, answer2=answers2 ,Nanswer1=0,Nanswer2=0,pub_date =timezone.now())
        return redirect('/')
    return render(request, 'add.html')

def answer_page(request, quest_id):
    quest_ = Quest.objects.get(id=quest_id)
    answer_ = Answer.objects.filter(question=quest_)
    if (request.method == 'POST' and request.POST.get('send_ans', '') == 'send_ans'):
        Answer.objects.create(select_ans=request.POST.get('ans', "0") ,question=quest_)
        if(request.POST.get('ans', "0")=="1"):
           quest_.Nanswer1=int(quest_.Nanswer1)+1
           quest_.save()
        elif request.POST.get('ans', "0")=="2":
           quest_.Nanswer2=int(quest_.Nanswer2)+1
           quest_.save()
        return redirect('/answer/%d' % int(quest_id))
    return render(request, 'answer.html', {'quest_': quest_,'answer_':answer_})
