from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'final_test.views.home_page', name='home'),
    url(r'^add$', 'final_test.views.add_Q', name='add'),
    url(r'^answer/(\d+)/$', 'final_test.views.answer_page', name='answer'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^admin/', include(admin.site.urls)),
)
