# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('final_test', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='select_ans',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
