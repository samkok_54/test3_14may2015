# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('final_test', '0002_auto_20150514_0804'),
    ]

    operations = [
        migrations.AddField(
            model_name='answer',
            name='answer1',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='answer',
            name='answer2',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
