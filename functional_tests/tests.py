from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.test import LiveServerTestCase
from django.utils import timezone

class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get(self.live_server_url)
        self.assertIn('Test3-5601012620064', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Test III', header_text)
        # click add Question and go to add page(add.html)
        self.browser.find_element_by_id('add_Q').click()
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('ADD QUESTION', header_text)
        inputQ = self.browser.find_element_by_id('id_question')
        inputA1 = self.browser.find_element_by_id('id_answer1')
        inputA2 = self.browser.find_element_by_id('id_answer2')
        # check placeholder of input tab
        self.assertEqual(
                inputQ.get_attribute('placeholder'),
                'type your question'
        )
        self.assertEqual(
                inputA1.get_attribute('placeholder'),
                'type your answer1'
        )
        self.assertEqual(
                inputA2.get_attribute('placeholder'),
                'type your answer2'
        )
        # input Question and Answer choice 
        inputQ.send_keys('คุณชอบสีอะไร')
        inputA1.send_keys('สีขาว')
        inputA2.send_keys('สีดำ')
        self.browser.find_element_by_id('submit_Q').click()
        # back to homepage can see question
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Test III', header_text)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn('1 คุณชอบสีอะไร', page_text)
        #เลือกตอบคำถามของ 1 คุณชอบสีอะไร
        self.browser.find_element_by_id('view_question_1').click()
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('ANS MY QUESTION', header_text)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn('สีขาว : 0', page_text)
        self.assertIn('สีดำ : 0', page_text)
        #vote สีขาว และเช็คว่ายังอยู่ที่หน้าเดิมคือหน้าตอบคำถาม
        self.browser.find_element_by_id('ans-2').click()
        self.browser.find_element_by_id('submit_A').click()
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('ANS MY QUESTION', header_text)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn('สีขาว : 1', page_text)
        self.assertIn('สีดำ : 0', page_text)
        #ตรวจสอบ link back ไปยัง home.html
        self.browser.find_element_by_id('back2home').click()
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Test III', header_text)


if __name__ == '__main__':
    unittest.main(warnings='ignore')
