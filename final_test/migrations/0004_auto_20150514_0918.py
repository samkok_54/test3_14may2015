# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('final_test', '0003_auto_20150514_0916'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='answer',
            name='answer1',
        ),
        migrations.RemoveField(
            model_name='answer',
            name='answer2',
        ),
        migrations.AddField(
            model_name='quest',
            name='Nanswer1',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='quest',
            name='Nanswer2',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
