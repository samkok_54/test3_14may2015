from django.db import models
from django.utils import timezone

class Quest(models.Model):
    question = models.TextField(default='')
    answer1 = models.TextField(default='')
    answer2 = models.TextField(default='')
    Nanswer1 = models.IntegerField(default=0)
    Nanswer2 = models.IntegerField(default=0)
    pub_date = models.DateTimeField(default=timezone.now())

class Answer(models.Model):
    question = models.ForeignKey(Quest, default=None)
    select_ans = models.TextField(default='0')
